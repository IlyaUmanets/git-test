# README #
### Main git commands ###

* git init - ініціалізація гіт репозиторія
* git status - перевірка стану файлів
* git add name_file - додавання файлу для коміта
* git commit -m 'name_commit' - створення з назвою name_commit
* git checkout -b new_branch - створення вітки з назвою new_branch
* git push origin name_branch - пуш файлів на репозиторій в name_branch
* git pull origin name_branch - стягування файлів з репозиторія в name_branch